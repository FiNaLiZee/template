/*
SQLyog Ultimate v13.1.1 (64 bit)
MySQL - 8.0.21 : Database - 4greens
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`4greens` /*!40100 DEFAULT CHARACTER SET utf8 */ /*!80016 DEFAULT ENCRYPTION='N' */;

USE `4greens`;

/*Table structure for table `articles` */

DROP TABLE IF EXISTS `articles`;

CREATE TABLE `articles` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `parent` int unsigned DEFAULT NULL,
  `alias` varchar(30) NOT NULL,
  `title` varchar(30) NOT NULL,
  `category` tinyint unsigned NOT NULL,
  `author` varchar(30) NOT NULL,
  `publication` year NOT NULL,
  `difficult` tinyint unsigned NOT NULL,
  `link` varchar(255) NOT NULL,
  `iframe_link` varchar(255) DEFAULT NULL,
  `description` text NOT NULL,
  `views` int unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ALIAS_KEY` (`alias`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

/*Data for the table `articles` */

insert  into `articles`(`id`,`parent`,`alias`,`title`,`category`,`author`,`publication`,`difficult`,`link`,`iframe_link`,`description`,`views`) values 
(1,1,'c1hour','C за час',1,'Гоша Дударь',2018,2,'https://www.youtube.com/watch?v=t0_IRViZcHs','https://www.youtube.com/embed/t0_IRViZcHs','Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias consequuntur debitis est explicabo minima obcaecati possimus quaerat recusandae, tempore velit.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias consequuntur debitis est explicabo minima obcaecati possimus quaerat recusandae, tempore velit.',0),
(2,3,'2','p3 2',0,'',0000,0,'',NULL,'',0),
(3,2,'3','p2 3 ',1,'',0000,0,'',NULL,'',0),
(4,2,'4','p2 4',1,'',0000,0,'',NULL,'',0),
(5,5,'5','p5 5',2,'',0000,0,'',NULL,'',0),
(6,4,'6','p4 6',2,'',0000,0,'',NULL,'',0),
(7,3,'7','p3 7',2,'',0000,0,'',NULL,'',0),
(8,1,'8','p1 8',0,'',0000,0,'',NULL,'',0),
(9,5,'9','p5 9',0,'',0000,0,'',NULL,'',0),
(10,5,'0','p5 10',0,'',0000,0,'',NULL,'',0);

/*Table structure for table `catalog` */

DROP TABLE IF EXISTS `catalog`;

CREATE TABLE `catalog` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `parent` int unsigned DEFAULT NULL,
  `alias` varchar(30) DEFAULT NULL,
  `title` varchar(30) NOT NULL,
  `category` tinyint unsigned NOT NULL,
  `demand` tinyint unsigned NOT NULL,
  `suitable` tinyint unsigned NOT NULL,
  `for_what` varchar(50) NOT NULL,
  `time` tinyint unsigned NOT NULL,
  `syntax` tinyint unsigned DEFAULT NULL,
  `abstraction` tinyint unsigned DEFAULT NULL,
  `dynamical_vars` tinyint unsigned DEFAULT NULL,
  `oop` tinyint unsigned DEFAULT NULL,
  `level_type` tinyint unsigned DEFAULT NULL,
  `description` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

/*Data for the table `catalog` */

insert  into `catalog`(`id`,`parent`,`alias`,`title`,`category`,`demand`,`suitable`,`for_what`,`time`,`syntax`,`abstraction`,`dynamical_vars`,`oop`,`level_type`,`description`) values 
(1,NULL,'c','C',0,1,0,'Драйвера, ОС, ПО, микроконтроллеры',6,0,0,0,1,0,'много много описания'),
(2,NULL,'js','JavaScript',0,1,1,'Frontend, верстка',3,0,1,1,1,2,'Js - это крайне востребованный язык программирования. Основной его особенностью является отсутствие строгой типизации и табуляции поэтому он часто используется помогите автор сайта держет меня в заложниках при написании клиентской части сайтов. '),
(3,2,'vue','Vue',1,1,1,'Frontend UI, SPA',2,NULL,NULL,NULL,NULL,NULL,'бла бла бла VUE это круто бла бла бла'),
(4,3,'nuxt','Nuxt',1,1,1,'ds',1,NULL,NULL,NULL,NULL,NULL,'Vue на стероидах'),
(5,2,'react','React',1,1,1,'Frontend apps',3,NULL,NULL,NULL,NULL,NULL,'его величество востребованный React.js                                                                                                                                                    ');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;