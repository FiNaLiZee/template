const {promisify} = require('util')
const mysql = require('mysql')
const db = mysql.createConnection({host: '127.0.0.1', user: 'root', database: '4greens'})

module.exports = promisify(db.query).bind(db)

// const mysql2 = require('mysql2')
// const con = mysql2.createConnection(
//   {host: '127.0.0.1', user: 'root', database: '4greens'}
// )
// module.exports = query = (str,params) => con.promise().query(str, params)
//   .then(([rows, fields]) => {
//     return rows
//   })
//   .catch(console.log)
//   .then(() => con.end())