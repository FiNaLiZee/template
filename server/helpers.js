module.exports = {
  json: (context) => context.length === 1 ? JSON.stringify(context[0]) : JSON.stringify(context),
  definition: (value, arrOfValues, colored, valueIsBool) => {
    arrOfValues = JSON.parse('[' + arrOfValues + ']')
    let colors = ['text-danger', 'text-warning', 'text-success']
    if (valueIsBool === true) colors.splice(0, 1)
    if (colored === true) return `<span class="h5 ${colors[arrOfValues.indexOf(arrOfValues[value])]}"> ${arrOfValues[value]} </span>`
    else return `<span class="h5">${arrOfValues[value]}</span>`
  }, endingOfTheWord: (value, arrOfValues) => {
    let remainder
    if (value > 20) remainder = value % 10
    arrOfValues = JSON.parse('[' + arrOfValues + ']')
    if ((remainder || value) === 1) return `${value} ${arrOfValues[0]}`
    else if (remainder || value >= 2 && remainder || value <= 4) return `${value} ${arrOfValues[1]}`
    else if (value || remainder >= 5 || remainder === 0) return `${value} ${arrOfValues[2]}`
  }, iff: (a, operator, b, opts) => {
    let bool = false
    if (operator === '==') bool = a === b
    else if (operator === '!=') bool = a !== b
    else if (operator === '>') bool = a > b
    else if (operator === '<') bool = a < b
    else if (operator === '<=') bool = a <= b
    else if (operator === '>=') bool = a >= b
    else throw `Unknown operator ${operator}`
    if (bool) {
      return opts.fn(this)
    } else {
      return opts.inverse(this)
    }
  }, length: (value) => value.length
}