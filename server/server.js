const express = require('express')
const app = express()
const expHbs = require('express-handlebars')
const fs = require('fs')
const files = fs.readdirSync(`${__dirname}/controllers/`)
const hbs = expHbs.create({
  extname: '.hbs',
  helpers: require('./helpers')
})
// Engine set
app.engine('.hbs', hbs.engine)
app.set('view engine', '.hbs')
app.set('views', 'public/views/')
app.use(express.static('public'))
// DEBUG adding to locals
app.locals.DEBUG = process.argv.includes('debug')
// controllers
files.forEach(el => {
  let current = require(`./controllers/${el}`)
  app.get(current.url, current.method)
})
// listen
app.listen(3000, () => console.log(`Local server listening on port 3000`))