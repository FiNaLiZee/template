const query = require('../query')
// const {inspect} = require('util')
// console.log(inspect(objTree, false, null, true))

module.exports = {
  url: '/articles/:alias',
  method: async (req, res) => {
    let catalog = await query('SELECT id, alias, title, parent FROM catalog')
    let articles = await query('SELECT id, alias, title, parent FROM articles')
    // obj for catalog init
    let objTree = {}
    catalog.forEach(e => objTree[e.id] = e)
    // build catalog obj as tree
    let add = (obj, name) => {
      Object.keys(obj)
        .map(key => obj[key])
        .filter(obj => obj.parent && objTree[obj.parent])
        .forEach(obj => {
          if (!objTree[obj.parent][name]) objTree[obj.parent][name] = []
          objTree[obj.parent][name].push(obj)
        })
    }
    add(articles, 'articles')
    add(objTree, 'childs')
    objTree = Object.values(objTree).filter(e => !e.parent);
    // array with alias for prev/next article links
    let sideArticles
    let index = articles.sort((a, b) => a.parent - b.parent).map(e => e.alias === req.params.alias).indexOf(true)
    if (!index) sideArticles = [null, articles[index + 1]]
    else if (index === articles.length - 1) sideArticles = [articles[index - 1], null]
    else sideArticles = [articles[index - 1], articles[index + 1]]
    // current article data from DB
    let article = (await query('SELECT catalog.alias AS parentAlias, articles.* FROM catalog INNER JOIN articles ON articles.parent = catalog.id WHERE articles.alias = ?', [req.params.alias]))[0]
    res.render('article', {article, objTree, sideArticles})
  }
}