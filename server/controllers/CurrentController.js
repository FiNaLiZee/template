const query = require('../query')

module.exports = {
  url: '/catalog/:alias',
  method: async (req, res) => {
    let current = await query('SELECT * FROM catalog WHERE alias = ?', [req.params.alias])
    let articles = await query('SELECT * FROM articles WHERE parent = ?', [current[0].id])
    let frameworks = await query('SELECT * FROM catalog WHERE parent = ?', [current[0].id])
    res.render('current', {current: current[0], articles, frameworks})
  }
}