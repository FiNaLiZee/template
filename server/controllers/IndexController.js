const getCatalog = require('../methods')

module.exports = {
  url: '/',
  method: async (req, res) => res.render('index', {catalog: await getCatalog()})
}