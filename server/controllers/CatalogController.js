const getCatalog = require('../methods')

module.exports = {
  url: '/catalog',
  method: async (req, res) => res.render('catalog', {catalog: await getCatalog()})
}