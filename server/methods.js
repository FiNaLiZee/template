const query = require('./query')

module.exports = getCatalog = async () => {
  let catalog = await query('SELECT * FROM catalog')
  for (let o of catalog) {
    o.countedCategories = [0, 0, 0];
    (await query(`SELECT category, COUNT(category) AS cnt FROM articles WHERE parent = ? GROUP BY category`, [o.id])).forEach(o2 => o.countedCategories[o2.category] = o2.cnt)
  }
  return catalog
}