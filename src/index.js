// Vue
// import Vue from "vue";
// Bootstrap
// import 'popper.js'
// import 'jquery'
// import 'bootstrap/dist/js/bootstrap.min'
// import 'bootstrap/dist/css/bootstrap.min.css'
// JS
// import './js/'

// SCSS
import './scss/main.scss'

global.$ = function (selector, startNode) {
  if (selector.length < 1) return null
  let node = (startNode) ? startNode : document
  let pref = selector[0], value = selector.substr(1)
  if (pref === '#') return node.getElementById(value)
  else if (pref === '.') return node.getElementsByClassName(value)
  else if (pref === '^') return node.getElementsByName(value)
  else if (pref === '_') return node.getElementsByTagName(value)
  else return node.getElementById(selector)
}

// Vue bus
Vue.prototype.bus = global.bus = new Vue()

// Auto import for Vue components
const components = require.context('./', true, /\w+\.(vue)$/)
components.keys().forEach(filename => {
  let config = components(filename)
  let name = filename.split('/').pop().replace(/\.\w+$/, '').replace(/([a-z])([A-Z])/g, '$1-$2').toLowerCase()
  Vue.component('v-' + name, config.default || config)
  console.log(filename, 'mapped by', 'v-' + name)
})

// Vue init
const app = new Vue({
  el: '#app'
})